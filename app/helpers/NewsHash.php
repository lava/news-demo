<?php

namespace app\helpers;

use app\models\News;

class NewsHash
{
    public static function generate(string $source, string $title, string $publishedAt, string $url = ''): string
    {
        return md5($source . $title . $publishedAt . $url);
    }

    public static function generateByNews(News $news): string
    {
        return md5($news->source . $news->title . $news->publishedAt . $news->url);
    }

    public static function validate(string $hash, string $source, string $title, string $publishedAt, string $url = ''): bool
    {
        $newsHash = self::generate($source, $title, $publishedAt, $url);
        return $hash = $newsHash;
    }
}
