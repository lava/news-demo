<?php

namespace app\traits;

use Yii;
use \yii\helpers\VarDumper;

trait LogTrait
{
    protected function getLogCategory()
    {
        return 'application';
    }

    /**
     * @param mixed $object
     * @param string $message
     * @return void
     */
    protected function dump($object, string $message = '', string $category = '')
    {
        if (empty($category)) $category = $this->getLogCategory();
        if (!empty($message)) {
            $message .= "\n";
        }
        Yii::info($message . VarDumper::dumpAsString($object), $category);
    }

    /**
     * Dump model's attributes
     *
     * @param object $model
     * @param string $message
     * @return void
     */
    protected function dumpModelAttributes($model, string $message = '', string $category = '')
    {
        if (empty($category)) $category = $this->getLogCategory();
        if (empty($message)) {
            $message = get_class($model) . " model's attributes:\n";
        }
        foreach ($model->attributes as $name => $value) {
            if (is_scalar($value)) {
                $message .= "$name = $value\n";
            } else {
                $message .= "$name = " . VarDumper::dumpAsString($value) . "\n";
            }
        }
        Yii::info($message, $category);
    }

    protected function dumpModelErrors($model, string $message = '', string $category = '')
    {
        if (empty($category)) $category = $this->getLogCategory();
        if (empty($message)) {
            $message = get_class($model) . " model's errors:\n";
        }
        foreach ($model->errors as $name => $errors) {
            foreach ($errors as $error) {
                $message .= "[$name] $error\n";
            }
        }
        Yii::info($message, $category);
    }

    protected function dumpModel($model, string $category = '')
    {
        if (empty($category)) $category = $this->getLogCategory();
        $this->dumpModelAttributes($model, '', $category);
        $this->dumpModelErrors($model, '', $category);
    }


    protected function dumpException(\Throwable $exception, string $category = '')
    {
        if (empty($category)) $category = $this->getLogCategory();
        Yii::error(
            '#' . $exception->getCode()
                . ' '
                . $exception->getMessage()
                . "\nTrace:\n" . $exception->getTraceAsString(),
            $category
        );
    }
}
