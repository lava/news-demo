<?php

namespace app\traits;

use Yii;
use yii\helpers\Console;

trait ConsoleWorkoutTrait
{
    protected function writeError($msg, $newLine = true)
    {
        if (is_array($msg)) {
            $msg = join("\n", $msg);
        }
        $eol = $newLine ? "\n" : '';
        $this->stdout($msg . $eol, Console::FG_RED);
    }

    protected function writeWarning($msg, $newLine = true)
    {
        $eol = $newLine ? "\n" : '';
        $this->stdout($msg . $eol, Console::FG_YELLOW);
    }

    protected function writeSuccess($msg, $newLine = true)
    {
        $eol = $newLine ? "\n" : '';
        $this->stdout($msg . $eol, Console::FG_GREEN);
    }

    protected function write($msg = '', $newLine = true)
    {
        $eol = $newLine ? "\n" : '';
        $this->stdout($msg . $eol);
    }

    protected function confirmDangerAction($msg, $default = false): bool
    {
        $msg = Console::ansiFormat($msg . ' (yes|no) [' . ($default ? 'yes' : 'no') . ']:', [Console::FG_RED]);
        while (true) {
            Console::stdout($msg);
            $input = trim(Console::stdin());

            if (empty($input)) {
                return $default;
            }

            if (!strcasecmp($input, 'y') || !strcasecmp($input, 'yes')) {
                return true;
            }

            if (!strcasecmp($input, 'n') || !strcasecmp($input, 'no')) {
                return false;
            }
        }
    }
}
