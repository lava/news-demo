<?php

namespace app\commands;

use Yii;
use DateTime;

use jcobhams\NewsApi\NewsApi;

use app\helpers\NewsHash;
use app\models\News;

use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\VarDumper;

class NewsController extends Controller
{
    use \app\traits\ConsoleWorkoutTrait;
    use \app\traits\LogTrait;

    private $pageSize = 50;
    private $apiClient = null;

    public function actionImport($query = 'framework')
    {
        $lastNews = News::getLast();
        $from = null;
        if ($lastNews) {
            $from = $this->convertNewsDateToApiDate($lastNews->publishedAt);
        }
        $response = $this->readPage($query, $from);
        if (!$response) {
            $this->writeError("API error");
            return ExitCode::IOERR;
        }

        $this->write("Total count: $response->totalResults");

        //берём только 1-ю страницу т.к. есть ограничения для бесплатного аккаунта
        $total = min($response->totalResults, $this->pageSize);
        $totalPageCount = ceil($total / $this->pageSize);

        $importedCount = 0;
        $skipedCount = 0;
        $this->write('Importing', false);
        for ($i = 0; $i < $totalPageCount; $i++) {
            $response = $this->readPage($query, $from, $this->pageSize, $i + 1);
            if (!$response) {
                $this->writeError("Failed to read page #{($i+1)}");
            } else {
                foreach ($response->articles as $article) {
                    $news = $this->createNewsIfNotExists($article);
                    if ($news) {
                        $importedCount++;
                    } else if ($news === null) {
                        $skipedCount++;
                    }
                    $this->write('.', false);
                }
            }
        }
        $this->writeSuccess("\nDone");
        $this->write("Imported $importedCount, skiped $skipedCount");
        return ExitCode::OK;
    }

    /**
     * Undocumented function
     *
     * @param object $article
     * @return News|null|false null if skiped, false on errors
     */
    private function createNewsIfNotExists($article)
    {
        $news = Yii::$app->news->createNewsIfNotExists([
            'source' => $article->source->name,
            'url' => $article->url,
            'author' => $article->author,
            'title' => $article->title,
            'description' => $article->description,
            'publishedAt' => $this->convertApiDateToNewsDate($article->publishedAt),
            'importedAt' => date('Y-m-d H:i:s'),
        ]);
        if (!$news) {
            Yii::error('Failed to create News');
            return false;
        }
        if ($news->save()) {
            return $news;
        }
        Yii::error('Failed to save News');
        $this->dumpModel($news);
        return false;
    }

    private function getApiClient(): NewsApi
    {
        if (!$this->apiClient) $this->apiClient = new NewsApi(Yii::$app->params['newsapiToken']);
        return $this->apiClient;
    }

    private function readPage(string $query, string $from = null, int $limit = 1, int $offset = 1): ?object
    {
        $response = $this->getApiClient()->getEverything(
            $query,
            null,
            null,
            null,
            $from,
            null,
            'en',
            null,
            $limit,
            $offset
        );
        // Yii::debug(VarDumper::dumpAsString($response));
        if ($response->status != 'ok') {
            return null;
        }
        return $response;
    }

    private function convertNewsDateToApiDate(string $date)
    {
        $apiDate = new DateTime($date);
        return $apiDate->format('c');
    }

    private function convertApiDateToNewsDate(string $apiDate)
    {
        $date = new DateTime($apiDate);
        return $date->format('Y-m-d H:i:s');
    }
}
