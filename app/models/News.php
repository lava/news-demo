<?php

namespace app\models;

use app\helpers\NewsHash;
use Yii;

/**
 * This is the model class for table "{{%news}}".
 *
 * @property int $id
 * @property string $hash
 * @property string|null $source
 * @property string|null $publishedAt
 * @property string|null $importedAt
 * @property string|null $author
 * @property string|null $title
 * @property string|null $url
 * @property string|null $description
 * 
 * @property NewsTags[] $newsTags
 * @property Tag[] $tags
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%news}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['hash', 'default', 'value' => function ($model, $attribute) {
                return $model->generateHash();
            }],
            [['hash'], 'required'],
            [['publishedAt', 'importedAt'], 'safe'],
            [['description'], 'string'],
            [['hash'], 'string', 'max' => 32],
            [['source', 'author', 'title', 'url'], 'string', 'max' => 255],
            [['hash'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hash' => 'Hash',
            'source' => 'Source',
            'publishedAt' => 'Published At',
            'importedAt' => 'Imported At',
            'author' => 'Author',
            'title' => 'Title',
            'url' => 'Url',
            'description' => 'Description',
        ];
    }

    /**
     * Gets query for [[NewsTags]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNewsTags()
    {
        return $this->hasMany(NewsTags::class, ['newsId' => 'id']);
    }

    /**
     * Gets query for [[Tags]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(Tag::class, ['id' => 'tagId'])->viaTable('{{%news_tags}}', ['newsId' => 'id']);
    }

    public function generateHash()
    {
        $this->hash = NewsHash::generate(
            $this->source,
            $this->title,
            $this->publishedAt,
            $this->url
        );
    }

    public static function getLast(): ?News
    {
        return self::find()
            ->orderBy(['publishedAt' => SORT_DESC])
            ->limit(1)
            ->one();
    }

    public static function findByHash(string $hash): ?News
    {
        return self::findOne(['hash' => $hash]);
    }
}
