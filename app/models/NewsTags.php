<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%news_tags}}".
 *
 * @property int $newsId
 * @property int $tagId
 *
 * @property News $news
 * @property Tag $tag
 */
class NewsTags extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%news_tags}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['newsId', 'tagId'], 'required'],
            [['newsId', 'tagId'], 'integer'],
            [['newsId', 'tagId'], 'unique', 'targetAttribute' => ['newsId', 'tagId']],
            [['newsId'], 'exist', 'skipOnError' => true, 'targetClass' => News::class, 'targetAttribute' => ['newsId' => 'id']],
            [['tagId'], 'exist', 'skipOnError' => true, 'targetClass' => Tag::class, 'targetAttribute' => ['tagId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'newsId' => 'News ID',
            'tagId' => 'Tag ID',
        ];
    }

    /**
     * Gets query for [[News]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasOne(News::class, ['id' => 'newsId']);
    }

    /**
     * Gets query for [[Tag]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTag()
    {
        return $this->hasOne(Tag::class, ['id' => 'tagId']);
    }


    public static function findByNewsAndTag(News $news, Tag $tag): NewsTags
    {
        return NewsTags::find()
            ->where(['newsId' => $news->id, 'tagId' => $tag->id])
            ->limit(1)
            ->one();
    }
}
