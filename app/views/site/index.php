<?php

/* @var $this yii\web\View */

use app\widgets\NewsWithTags;

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <h1><?= $this->title ?></h1>

    <?= NewsWithTags::widget(['tags' => ['fire']]) ?>
</div>