<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%news_tags}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%news}}`
 * - `{{%tags}}`
 */
class m210324_195450_create_junction_news_and_tags_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%news_tags}}', [
            'newsId' => $this->integer(),
            'tagId' => $this->integer(),
            'PRIMARY KEY(newsId, tagId)',
        ]);

        // creates index for column `newsId`
        $this->createIndex(
            '{{%idx-news_tags-newsId}}',
            '{{%news_tags}}',
            'newsId'
        );

        // add foreign key for table `{{%news}}`
        $this->addForeignKey(
            '{{%fk-news_tags-newsId}}',
            '{{%news_tags}}',
            'newsId',
            '{{%news}}',
            'id',
            'CASCADE'
        );

        // creates index for column `tagId`
        $this->createIndex(
            '{{%idx-news_tags-tagId}}',
            '{{%news_tags}}',
            'tagId'
        );

        // add foreign key for table `{{%tags}}`
        $this->addForeignKey(
            '{{%fk-news_tags-tagId}}',
            '{{%news_tags}}',
            'tagId',
            '{{%tags}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%news}}`
        $this->dropForeignKey(
            '{{%fk-news_tags-newsId}}',
            '{{%news_tags}}'
        );

        // drops index for column `newsId`
        $this->dropIndex(
            '{{%idx-news_tags-newsId}}',
            '{{%news_tags}}'
        );

        // drops foreign key for table `{{%tags}}`
        $this->dropForeignKey(
            '{{%fk-news_tags-tagId}}',
            '{{%news_tags}}'
        );

        // drops index for column `tagId`
        $this->dropIndex(
            '{{%idx-news_tags-tagId}}',
            '{{%news_tags}}'
        );

        $this->dropTable('{{%news_tags}}');
    }
}
