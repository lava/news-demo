<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%news}}`.
 */
class m210324_142922_create_news_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%news}}', [
            'id' => $this->primaryKey(),
            'hash' => $this->string(32)->notNull()->unique(),
            'source' => $this->string(),
            'publishedAt' => $this->datetime(),
            'importedAt' => $this->datetime(),
            'author' => $this->string(),
            'title' => $this->string(),
            'description' => $this->text(),
            'url' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%news}}');
    }
}
