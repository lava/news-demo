<?php

namespace app\components;

use Yii;
use Exception;
use Throwable;

use app\models\News;
use app\models\NewsTags;
use app\models\Tag;
use yii\base\Component;

class NewsService extends Component
{
    use \app\traits\LogTrait;

    public function createNews(array $attrs): ?News
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $item = new News();
            $item->setAttributes($attrs);
            if (!$item->save()) {
                $this->dumpModel($item);
                throw new Exception('Failed to save News');
            }
            if (!$this->createTagsFromNews($item)) {
                throw new Exception('Failed to create news`s tags');
            }
            $transaction->commit();
            return $item;
        } catch (Throwable $e) {
            $this->dumpException($e);
            $transaction->rollBack();
            return null;
        }
    }

    public function createNewsIfNotExists(array $attrs): ?News
    {
        $item = new News();
        $item->setAttributes($attrs);
        $item->generateHash();
        if ($this->newsExists($item->hash)) {
            return News::findByHash($item->hash);
        }
        try {
            $transaction = Yii::$app->db->beginTransaction();

            if (!$item->save()) {
                $this->dumpModel($item);
                throw new Exception('Failed to save News');
            }
            if (!$this->createTagsFromNews($item)) {
                throw new Exception('Failed to create news`s tags');
            }
            $transaction->commit();
            return $item;
        } catch (Throwable $e) {
            $this->dumpException($e);
            $transaction->rollBack();
            return null;
        }
    }

    public function newsExists(string $hash): bool
    {
        return News::find()->where(['hash' => $hash])->exists();
    }

    public function createTagIfNotExists(array $attrs): ?Tag
    {
        $name = $attrs['name'] ?? '';
        if (empty($name)) {
            throw new Exception('Empty tag name');
        }
        if ($this->tagExists($name)) {
            $item = Tag::findByName($name);
            $item->updateCounters(['totalCount' => 1]);
            return $item;
        }
        try {
            $transaction = Yii::$app->db->beginTransaction();
            $item = $this->createTag($attrs);
            if (!$item) {
                throw new Exception('Failed to create Tag');
            }
            $transaction->commit();
            return $item;
        } catch (Throwable $e) {
            $this->dumpException($e);
            $transaction->rollBack();
            return null;
        }
    }

    public function createTag(array $attrs): ?Tag
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $item = new Tag();
            if (!isset($attrs['totalCount'])) $attrs['totalCount'] = 1;
            $item->setAttributes($attrs);
            if (!$item->save()) {
                $this->dumpModel($item);
                throw new Exception('Failed to save Tag');
            }
            $transaction->commit();
            return $item;
        } catch (Throwable $e) {
            $this->dumpException($e);
            $transaction->rollBack();
            return null;
        }
    }

    public function tagExists(string $name): bool
    {
        return Tag::find()->where(['name' => $name])->exists();
    }

    public function attachTagToNews(News $news, Tag $tag): ?NewsTags
    {
        if ($this->isTagAttachedToNews($news, $tag)) {
            return NewsTags::findByNewsAndTag($news, $tag);
        }
        try {
            $transaction = Yii::$app->db->beginTransaction();
            $item = new NewsTags();
            $item->newsId = $news->id;
            $item->tagId = $tag->id;
            if (!$item->save()) {
                $this->dumpModel($item);
                throw new Exception('Failed to save NewsTags');
            }
            $transaction->commit();
            return $item;
        } catch (Throwable $e) {
            $this->dumpException($e);
            $transaction->rollBack();
            return null;
        }
    }

    public function dettachTagFromNews(News $news, Tag $tag): bool
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            NewsTags::deleteAll(['newsId' => $news->id, 'tagId' => $tag->id]);
            $transaction->commit();
            return true;
        } catch (Throwable $e) {
            $this->dumpException($e);
            $transaction->rollBack();
            return false;
        }
    }

    public function isTagAttachedToNews(News $news, Tag $tag): bool
    {
        return NewsTags::find()->where(['newsId' => $news->id, 'tagId' => $tag->id])->exists();
    }


    private function createTagsFromNews(News $news): bool
    {
        $tagsArray = $this->parseTags($news->title);
        foreach ($tagsArray as $name) {
            $tag = $this->createTagIfNotExists(['name' => $name]);
            if (!$tag) return false;
            if (!$this->attachTagToNews($news, $tag)) return false;
        }
        return true;
    }

    private function parseTags(string $data): array
    {
        $data = preg_replace('/\W/', ' ', $data);
        $tags = preg_split('/\s+/', $data);
        if (empty($tags)) return [];
        return $tags;
    }

    /**
     * Undocumented function
     *
     * @param array $tags
     * @return News[]
     */
    public function getNewsWithOneOfTags(array $tags): array
    {
        if (!$tags) return [];
        return News::find()
            ->alias('n')
            ->joinWith('tags t')
            ->where(['t.name' => $tags])
            ->all();
    }
}
