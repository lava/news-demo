<?php

namespace app\widgets;

use Yii;
use yii\bootstrap\Html;
use yii\bootstrap\Widget;

class NewsWithTags extends Widget
{
    public $tags = [];

    public function run()
    {
        if (empty($this->tags)) return '';
        $news = Yii::$app->news->getNewsWithOneOfTags($this->tags);
        if (empty($news)) return '';
        $tagsList = implode(', ', $this->tags);
        $html = '';
        $html .= Html::beginTag('div', ['class' => 'news-with-tag-widget']);
        $html .= Html::tag('h5', 'News with tags: ' . $tagsList);
        $items = [];
        foreach ($news as $item) {
            $items[] = $item->title;
        }
        $html .= Html::ul($items);
        $html .= Html::endTag('div');
        return $html;
    }
}
