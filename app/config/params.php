<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',

    'newsapiToken' => 'd3e8c43be40f4930b5c77c817d5ddb6e',
];
